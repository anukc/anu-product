// import React, { useState } from 'react';
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import "./App.css";
import Header from "./components/header/header";
import Products from "./components/products/products";

import Checkout from "./components/checkout/checkout";
import AddToCart from "./components/cart/AddToCart";
import React from "react";

class App extends React.Component {
  state = {
    total: 0,
    items: [],
  };

  valueCalculate = (name, price) => {
    const totalPrice = this.state.total + price;
    this.setState({ ...this.state, total: totalPrice });
    this.state.items.push(name);
  };




  render() {
    return (
      <div className="App">

        <Router>

          <Switch>
            <Route exact path="/checkout" >
              <Checkout total={this.state.total} items={this.state.items} />
            </Route>
            <Route exact path="/" >
              <Header />
              <Products valueCalculate={this.valueCalculate} />
              <AddToCart />
            </Route>

          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;

