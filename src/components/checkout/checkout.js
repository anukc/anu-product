//import { render } from "@testing-library/react";
import React from "react";
import { CartContext } from "../products/individualProducts";
import { Component } from "react";

class Checkout extends Component {

  render() {
    const { total, items } = this.props;
    return (
      <div>
        <header>Checkout Page</header>
        <h3>Items bought</h3>
        {items.map((item, idx) => (
          <p>{idx + 1}. {item}</p>
        ))}
        <p>Total: {total}</p>

      </div>
    );
  }
}

export default Checkout;
