import React, { Component, createContext } from "react";
import { BrowserRouter, Router, Link, Switch } from "react-router-dom";
import { useHistory } from "react-router";
function IndividualProducts({ name, price, src, valueCalculate }) {


  return (
    <div className="col-md-3">
      <div className="product-image-content">
        <img className="product-image" alt={name} src={src} />
      </div>
      <p>{name} </p>
      <p>Rs. {price} </p>
      <div>
        <button className="btn btn-primary" onClick={() => valueCalculate(name, Number(price))}>
          Add to Cart
            </button>
      </div>
    </div>
  );
}

export default IndividualProducts;
