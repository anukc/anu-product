import React, { Component, createContext } from "react";
import IndividualProducts from "./individualProducts";


class Products extends Component {

  render() {
    const { valueCalculate } = this.props;

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="row">
              <IndividualProducts
                name="Coastal"
                price="100"
                valueCalculate={valueCalculate}
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSl-p6hTfg_rnBr3UIcE58GcMZ3hYEq5YUUTpdzydLqRzAnDZemCRTt2ucWSPZubxbOQu4&usqp=CAU"
              />
              <IndividualProducts
                name="Dufresne"
                price="500"
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZKWLDMB5xrbIFsYeqQR1c1NtXrKamDqrCSefUmUamcMeQCrVnFhvvkw2Ud9Ewi0YpWy0&usqp=CAU"
                valueCalculate={valueCalculate}

              />
              <IndividualProducts
                name="Bohemian"
                price="1000"
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7JWlF9u6TyhWgPGxhL21NbQaLiaeca9GDM-ZOw4j6XkiNuVFrqHJr0-aZH5GAbryUkrM&usqp=CAU"
                valueCalculate={valueCalculate}

              />
              <IndividualProducts
                name="Upholstered"
                price="600"
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRJIBTho2fCd56bBCaO7uECKKMBUMFUzrFSSA&usqp=CAU"
                valueCalculate={valueCalculate}

              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Products;
