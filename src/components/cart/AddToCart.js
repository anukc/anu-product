import React from "react";
import { Route, BrowserRouter as Router, Switch, Link } from "react-router-dom";
import Checkout from '../checkout/checkout'


function AddToCart() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <div className="checkout-button">
            <Link to="/checkout">

              <button className="btn btn-danger">Checkout</button>

            </Link>

            {/* <Route path="/checkout" Component={Checkout}></Route>   */}

          </div>
        </div>
      </div>
    </div >
  );
}
export default AddToCart;
//ReactDOM.render(<BrowserRouter><App></App></BrowserRouter>, document.getElementById("root"));
